﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Preloader : MonoBehaviour {

    public GameObject GameManagerObject;
    public GameObject UIManagerObject;

    private void Awake()
    {
        if( GameplayManager.Instance == null )
        {
            Instantiate(GameManagerObject, Vector3.zero, Quaternion.identity);
        }

        if( UIManager.Instance == null)
        {
            Instantiate(UIManagerObject, Vector3.zero, Quaternion.identity);
        }
    }
}
