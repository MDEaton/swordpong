﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemySpawnBoxDetector : MonoBehaviour {

    List<GameObject> EnemiesInsideMe = new List<GameObject>();

    private void OnTriggerEnter(Collider collision)
    {
        EnemiesInsideMe.Add(collision.gameObject);
    }

    private void OnTriggerExit(Collider collision)
    {
        EnemiesInsideMe.Remove(collision.gameObject);
    }

    public bool ReturnEnemyCount ()
    {
        Debug.Log("MY SIZE: " + EnemiesInsideMe.Count + " My Name: " + gameObject.name);

        for( int x = 0; x < EnemiesInsideMe.Count; x++)
        {
            if( EnemiesInsideMe[x] == null )
            {
                EnemiesInsideMe.RemoveAt(x);
            }
        }

        if (EnemiesInsideMe.Count == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
