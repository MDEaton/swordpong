﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public int StartHealth = 1;
    public Vector2 GoldOnDeath = new Vector2();
    int CurrentHealth;

    float DamageCooldown = 1f;
    float CD_COUNTER;

    public GameObject BLOOD;
    WaveManager parentWaveManager;

    void Awake()
    {
        transform.Rotate(0.0f, Random.Range(0.0f, 360.0f),0f);
    }

    // Use this for initialization
    void Start () {
        CurrentHealth = StartHealth;
        parentWaveManager = gameObject.transform.parent.GetComponent<WaveManager>();
    }

    private void Update()
    {
        if (CD_COUNTER > 0f)
        {
            CD_COUNTER -= Time.deltaTime;
        }
    }


    public void TakeDamage (int dmg)
    {
        if( CD_COUNTER <= 0f)
        {
            CurrentHealth -= dmg;
            CD_COUNTER = DamageCooldown;
        }
        if( CurrentHealth <= 0 )
        {
            parentWaveManager.RemoveMe(this.gameObject);
            parentWaveManager.EnemyDied(Random.Range((int)GoldOnDeath.x, (int)GoldOnDeath.y));
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Sword"))
        {
            if (CD_COUNTER <= 0f)
            {
                Debug.Log("HIT By " + collision.gameObject.name);
                TakeDamage(1);
                Instantiate(BLOOD, gameObject.transform.position, Quaternion.identity);
            }
        }
    }

}
