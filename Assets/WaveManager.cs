﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour {

    public EnemySpawnBoxDetector[] SpawnPoints;
    public EnemyWaves[] LevelWaves;
    List<int> EnemyWaveLengths = new List<int>();
    int currentEnemyCount = 0;
    int currentWaveCount = 0;

    bool LastWave = false;
    bool LastEnemy = false;

    List<GameObject> ActiveEnemies = new List<GameObject>();
    public int MaxActiveEnemies = 3;

    float TIME_TO_SPAWN = 5f;
    float START_OF_WAVE_TIME = 0.25f;
    float spawnTimeCounter;

    int EnemiesSlain;
    int GoldGathered;

	// Use this for initialization
	void Start ()
    {
        EnemiesSlain = 0;
        GoldGathered = 0;
	    for(int x = 0; x < LevelWaves.Length; x++)
        {
            EnemyWaveLengths.Add(LevelWaves[x].EnemyCount());
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if( GameplayManager.Instance.CURRENT_GAME_STATE == GameplayManager.GameState.Play)
        {
            //Debug.LogError(LevelWaves.Length + "  .  " + currentWaveCount);
            //Debug.LogError(EnemyWaveLengths[currentWaveCount] + " ! " + currentEnemyCount);
            if (currentWaveCount >= LevelWaves.Length)
            {
                //Debug.LogError("LEVEL OVER");
                GameplayManager.Instance.LevelFinished();
            }
            else if (currentEnemyCount < EnemyWaveLengths[currentWaveCount] && currentWaveCount < LevelWaves.Length)
            {
                if (spawnTimeCounter > 0f && ActiveEnemies.Count < MaxActiveEnemies)
                {
                    spawnTimeCounter -= Time.deltaTime;
                }
                else if (spawnTimeCounter <= 0f && ActiveEnemies.Count < MaxActiveEnemies)
                {
                    SpawnEnemy();
                }
            }
            else if( currentEnemyCount >= EnemyWaveLengths[currentWaveCount] && currentWaveCount < LevelWaves.Length)
            {
                //Debug.LogWarning("NO ENEMIES TO SPAWN");
                if (ActiveEnemies.Count <= 0)
                {
                    currentWaveCount++;
                    currentEnemyCount = 0;
                    //Debug.LogWarning("NEW WAVE");
                }
            }


            //Debug.LogWarning(ActiveEnemies.Count);
        }
	}

    void SpawnEnemy()
    {
        EnemySpawnBoxDetector SpawnBox = SpawnPoints[Random.Range(0, SpawnPoints.Length)];
        if (SpawnBox.ReturnEnemyCount() )
        {
            GameObject NewEnemy = Instantiate(LevelWaves[currentWaveCount].GetEnemy(currentEnemyCount), SpawnBox.gameObject.transform.position, Quaternion.identity);
            ActiveEnemies.Add(NewEnemy);
            NewEnemy.transform.parent = gameObject.transform;
            currentEnemyCount++;
        }
    }

    public void StartSpawns ()
    {

    }

    public void RemoveMe (GameObject deadEnemy)
    {
        ActiveEnemies.Remove(deadEnemy);
    }

    public void EnemyDied(int GoldAmount)
    {
        EnemiesSlain++;
        GoldGathered += GoldAmount;
        UIManager.Instance.UpdateGold(GoldGathered);
        UIManager.Instance.SetSummaryScores(GoldGathered, EnemiesSlain);
    }
}
