﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour {

    public enum GameState {  MainMenu = 0, Countdown, Play, Pause, Failure, Victory};
    public GameState CURRENT_GAME_STATE = GameState.MainMenu;

    private static GameplayManager _instance;
    public static GameplayManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<GameplayManager>();

            return _instance;
        }
    }

    WaveManager CurrentWaveManager;

    // Use this for initialization
    void Awake () {
        DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void LoadLevel (int LevelID, bool isLevel)
    {
        SceneManager.LoadScene(LevelID);
        if( isLevel ) //check if we're loading a level to do cleanup and countdown. otherwise, do nothing, load menus, whatever
        {
            PlayerManager.Instance.ResetPlayerStatus();
            CURRENT_GAME_STATE = GameState.Countdown;
            UIManager.Instance.Countdown(3);
        }
        
    }

    public void PlayerGameOver ()
    {
        CURRENT_GAME_STATE = GameState.Failure;
        UIManager.Instance.SwitchToMainMenu();
        LoadLevel(0,false);
    }

    public void LevelFinished()
    {
        CURRENT_GAME_STATE = GameState.Victory;
        UIManager.Instance.SwitchToScoreScreen();
    }

    public void Play (bool isLevelStart)
    {
        if( isLevelStart )
        {
            PlayerManager.Instance.FindSpawn();
            CurrentWaveManager = GameObject.FindGameObjectWithTag("WaveManagerOBJ").GetComponent<WaveManager>();
        }
        CURRENT_GAME_STATE = GameState.Play;
    }



}
