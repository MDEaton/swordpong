﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour {

    private static UIManager _instance;
    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UIManager>();
            }
            return _instance;
        }
    }

    public GameObject MainMenuUI;


    //public Image TextBG;
    public GameObject GamePlayUI;
    public TextMeshProUGUI LivesBox;
    public TextMeshProUGUI GoldBox;
    public TextMeshProUGUI CountdownBox;

    public GameObject SummaryScreenUI;
    public TextMeshProUGUI SummaryGoldCount;
    public TextMeshProUGUI SummaryEnemyCount;

    float CountdownTimer;
    bool CountdownFinished = true;

    int EnemiesSlain;
    int GoldGathered;

    private void Update()
    {
        if( GameplayManager.Instance.CURRENT_GAME_STATE == GameplayManager.GameState.MainMenu)
        {
            
        }
        else if( GameplayManager.Instance.CURRENT_GAME_STATE == GameplayManager.GameState.Countdown )// || GameplayManager.Instance.CURRENT_GAME_STATE == GameplayManager.GameState.Countdown)
        {
            if (CountdownTimer > 0f && !CountdownFinished)
            {
                CountdownTimer -= Time.deltaTime;
                int roundedTimer = (int)CountdownTimer;
                CountdownBox.SetText(roundedTimer.ToString());
            }
            else if (CountdownTimer <= 0f && !CountdownFinished)
            {
                CountdownFinished = true;
                GameplayManager.Instance.Play(true);//for starting game, true to spawn player, false for pause
                CountdownBox.SetText("");
            }
        }
        else if(GameplayManager.Instance.CURRENT_GAME_STATE == GameplayManager.GameState.Victory)
        {
            if( Input.anyKeyDown )
            {
                GameplayManager.Instance.PlayerGameOver();
            }
        }
        
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        SwitchToMainMenu();
    }

    public void UpdateGold (int NewAmount)
    {
        GoldBox.text = "Gold: " + NewAmount.ToString();
    }

    public void UpdateLives(int NewAmount)
    {
        LivesBox.text = "Lives: " + NewAmount.ToString();
    }

    public void Countdown( int StartingNumber)
    {
        CountdownTimer = StartingNumber;
        CountdownFinished = false;
        CountdownBox.SetText(CountdownTimer.ToString());
    }

    public void PlayButton ()
    {
        SwitchToGameplayUI();
        GameplayManager.Instance.LoadLevel(1, true);
    }

    public void SwitchToMainMenu()
    {
        HideAll();
        MainMenuUI.SetActive(true);
    }

    public void SwitchToGameplayUI ()
    {
        HideAll();
        GamePlayUI.SetActive(true);
    }

    public void SwitchToScoreScreen()
    {
        HideAll();
        SummaryScreenUI.SetActive(true);
        SummaryGoldCount.text = GoldGathered.ToString();
        SummaryEnemyCount.text = EnemiesSlain.ToString();
    }

    void HideAll()
    {
        GamePlayUI.SetActive(false);
        MainMenuUI.SetActive(false);
        SummaryScreenUI.SetActive(false);
    }

    public void SetSummaryScores (int Golds, int Enemies)
    {
        GoldGathered = Golds;
        EnemiesSlain = Enemies;
    }
}
