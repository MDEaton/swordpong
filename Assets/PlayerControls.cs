﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {

    public enum MoveAxis { none = 0, LeftRight, ForwardBack};
    public MoveAxis myAxis = MoveAxis.none;


    float leftBarrier = 3f; //x
    float rightBarrier = 18f; //x
    float topBarrier = -4f; //z
    float bottomBarrier = -17f; //z

    float movespeed = 15f;
    float CURSOR_movespeed = 0.025f;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        float cursor_x = Input.GetAxis("MouseX");
        float cursor_y = Input.GetAxis("MouseY");


        if ( myAxis == MoveAxis.ForwardBack)
        {
            

            if ( Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Vector2 ScreenTouch = Input.GetTouch(0).deltaPosition;
                gameObject.transform.Translate( ScreenTouch.y * CURSOR_movespeed,0f,0f);
            }
            /*
            if( Input.touchCount > 0 )
            {
                cursor_y = Input.touches[0].deltaPosition.y;
                
            }*/
            //gameObject.transform.Translate(0f, -cursor_y * CURSOR_movespeed, 0f);

            if (Input.GetKey(KeyCode.S))
            {
                if (gameObject.transform.position.z > bottomBarrier)
                {
                    Vector3 tempVec = gameObject.transform.position - (Vector3.forward * movespeed * Time.deltaTime);
                    gameObject.transform.position = tempVec;
                }
            }
            else if (Input.GetKey(KeyCode.W))
            {
                if (gameObject.transform.position.z < topBarrier)
                {
                    Vector3 tempVec = gameObject.transform.position + (Vector3.forward * movespeed * Time.deltaTime);
                    gameObject.transform.position = tempVec;
                }
            }
        }


        if (myAxis == MoveAxis.LeftRight)
        {
            /*if (Input.touchCount > 0)
            {
                
                cursor_x = Input.touches[0].deltaPosition.x;
                
            }
            gameObject.transform.Translate(-cursor_x * CURSOR_movespeed, 0f, 0f);
            */
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Vector2 ScreenTouch = Input.GetTouch(0).deltaPosition;
                gameObject.transform.Translate(-ScreenTouch.x * CURSOR_movespeed, 0f, 0f);
            }
            if (Input.GetKey(KeyCode.A))
            {
                if (gameObject.transform.position.x > leftBarrier)
                {
                    Vector3 tempVec = gameObject.transform.position - (Vector3.right * movespeed * Time.deltaTime);
                    gameObject.transform.position = tempVec;
                }
            }
            else if (Input.GetKey(KeyCode.D))
            {
                if (gameObject.transform.position.x < rightBarrier)
                {
                    Vector3 tempVec = gameObject.transform.position + (Vector3.right * movespeed * Time.deltaTime);
                    gameObject.transform.position = tempVec;
                }
            }
            
        }

        
    }
}
