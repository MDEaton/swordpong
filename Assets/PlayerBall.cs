﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBall : MonoBehaviour {

    public float MoveSpeed = 5f;
    float MaxSpeed = 15f;
    public Vector3 StartVector = Vector3.zero;
    Vector3 MovementVector;

    float rotateSpeed = 1.5f;
    float moveSpeedIncrements = 0.2f;

    float LastHitTimer = 1f;
    float LastHitCounter;
    GameObject LastPaddleHit;

	// Use this for initialization
	void Start () {
        MovementVector = StartVector;
	}
	
	// Update is called once per frame
	void Update () {
        if( GameplayManager.Instance.CURRENT_GAME_STATE == GameplayManager.GameState.Play )
        {
            gameObject.transform.position += MovementVector * MoveSpeed * Time.deltaTime;
            gameObject.transform.Rotate(Vector3.up, rotateSpeed * MoveSpeed);
        }

        if( LastHitCounter > 0f && LastPaddleHit != null)
        {
            LastHitCounter -= Time.deltaTime;
        }
        else if( LastHitCounter <= 0f && LastPaddleHit != null)
        {
            LastPaddleHit = null;
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        if( collision.gameObject.layer == LayerMask.NameToLayer("Collidables") && collision.gameObject.tag != "Paddle")
        {
            Debug.Log("HIT WALL");
            ReflectVector(collision);
        }
        else if( collision.gameObject.layer == LayerMask.NameToLayer("Collidables") && collision.gameObject.tag == "Paddle")
        {
            if ( LastPaddleHit == null )
            {
                LastPaddleHit = collision.gameObject;
                ReflectVector(collision);
                LastHitCounter = LastHitTimer;
            }
        }
        

        
    }

    private void ReflectVector( Collision collision )
    {
        Vector3 PointOfCollision = collision.contacts[0].point;
        Vector3 NewVector = Vector3.Reflect(MovementVector, collision.contacts[0].normal);
        NewVector.y = 0f;
        MoveSpeed += moveSpeedIncrements;
        if (MoveSpeed > MaxSpeed)
        {
            MoveSpeed = MaxSpeed;
        }
        MovementVector = NewVector;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.LogWarning("Trigger");
        if( other.gameObject.layer == LayerMask.NameToLayer("DeathLayer"))
        {
            PlayerManager.Instance.PlayerDied();
        }
    }
}
