﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMeTimer : MonoBehaviour {

    public float LifeTime = 1f;
    float DeathTimer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        DeathTimer += Time.deltaTime;
        if( DeathTimer >= LifeTime)
        {
            Destroy(gameObject);
        }
	}
}
