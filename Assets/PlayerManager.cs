﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public GameObject Player;

    int NumberOfLives = 3;
    float RESPAWN_TIME = 3;
    float RespawnCounter = 0f;
    bool PlayerRespawned = true;

    GameObject PlayerReference;

    private static PlayerManager _instance;
    public static PlayerManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<PlayerManager>();

            return _instance;
        }
    }

    int CURRENT_LIVES_LEFT;
    int CURRENT_GOLD;

    Transform SpawnPoint;
	


	// Update is called once per frame
	void Update ()
    {
        if( GameplayManager.Instance.CURRENT_GAME_STATE == GameplayManager.GameState.Play )
        {
            if (RespawnCounter > 0f && !PlayerRespawned)
            {
                RespawnCounter -= Time.deltaTime;
            }
            else if (RespawnCounter <= 0f && !PlayerRespawned)
            {
                PlayerReference = Instantiate(Player, SpawnPoint.position, Quaternion.identity);
                PlayerRespawned = true;
            }
        }
	}

    public void ResetPlayerStatus ()
    {
        CURRENT_LIVES_LEFT = NumberOfLives;
        CURRENT_GOLD = 0;
        //FindSpawn();
        UpdateUI();
    }

    public void PlayerDied()
    {
        Destroy(PlayerReference);
        CURRENT_LIVES_LEFT--;
        if( CURRENT_LIVES_LEFT <= 0)
        {
            GameplayManager.Instance.PlayerGameOver();
        }
        else
        {
            RespawnCounter = RESPAWN_TIME;
            PlayerRespawned = false;
        }
        UpdateUI();
    }

    public void AddGold (int Amount)
    {
        CURRENT_GOLD += Amount;
        UpdateUI();
    }

    void UpdateUI ()
    {
        UIManager.Instance.UpdateGold(CURRENT_GOLD);
        UIManager.Instance.UpdateLives(CURRENT_LIVES_LEFT);
    }

    public void FindSpawn()
    {
        SpawnPoint = GameObject.FindGameObjectWithTag("Spawn").transform;
        PlayerReference = Instantiate(Player, SpawnPoint.position, Quaternion.identity);
    }
}
