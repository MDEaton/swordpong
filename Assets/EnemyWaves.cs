﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaves : MonoBehaviour {

    public GameObject[] Enemies;
    int NumberOfEnemiesSpawned = 0;

    public int EnemyCount()
    {
        return Enemies.Length;
    }

    public GameObject GetEnemy (int EnemyToSpawn)
    {
        return Enemies[EnemyToSpawn];
    }
}
